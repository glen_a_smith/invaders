'use strict';

var Invaders = {
    showDebug: false
};


Invaders.Boot = function(game) {
    
  function preload() {
    // Here we load assets required for our preloader
    this.load.spritesheet('splash', 'assets/images/invader2.png');
    this.load.image('preloaderBar', 'assets/images/preloader-bar.png');

  }
  
  function create() {
    this.game.stage.backgroundColor = '#001b07';
    
    //  Unless you specifically know your game needs to support multi-touch I would recommend setting this to 1
    this.input.maxPointers = 1;

    this.scale.pageAlignHorizontally = true;
    if (this.game.device.desktop) {
      //  If you have any desktop specific settings, they can go in here

    } else {
      //  Same goes for mobile settings.
      //  In this case we're saying "scale the game, no lower than 256x384 and no higher than 2048x1536"
      
      this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
      this.scale.minWidth = 256;
      this.scale.minHeight = 384;
      this.scale.maxWidth = 2048;
      this.scale.maxHeight = 1536;
      this.scale.forceLandscape = true;
      this.scale.pageAlignHorizontally = true;

    }

    //  By this point the preloader assets have loaded to the cache, we've set the game settings
    //  So now let's start the real preloader going
    this.state.start('Preloader');
  }
  
  return {
      preload: preload,
      create: create
  }
    
    
    
    
};

