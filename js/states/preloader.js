

Invaders.Preloader = function() {
    
    function init() {

        this.physics.startSystem(Phaser.Physics.ARCADE);

    }

    function preload() {

        //  Display our preloader bar and a matching invader
        var invader = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'splash', 0);
        invader.anchor.setTo(0.5);
        invader.scale.setTo(10);
        var preloadBar = this.add.sprite(this.game.world.centerX, this.game.world.centerY + 128, 'preloaderBar');
        preloadBar.anchor.setTo(0.5, 0.5);
        this.load.setPreloadSprite(preloadBar);

        this.load.bitmapFont('minecraftia', 'assets/fonts/minecraftia.png', 'assets/fonts/minecraftia.xml');

        this.load.image('blank', 'assets/images/blank.png');
        this.load.image('base', 'assets/images/base.png');
        this.load.image('ufo','assets/images/ufo.png'); 
        this.load.image('bullet-ship', 'assets/images/bullet-ship.png');
        this.load.spritesheet('logo', 'assets/images/invader2.png', 16, 16);

        this.load.spritesheet('explosion','assets/images/explosion.png', 16, 16);
        this.load.spritesheet('explosion-ufo','assets/images/explosion-ufo.png', 16, 16);
        this.load.spritesheet('invader1', 'assets/images/invader1.png', 16, 16);
        this.load.spritesheet('invader2', 'assets/images/invader2.png', 16, 16);
        this.load.spritesheet('invader3', 'assets/images/invader3.png', 16, 16);
        this.load.spritesheet('ship', 'assets/images/ship.png', 16, 16);
        this.load.spritesheet('bullet-alien', 'assets/images/bullet-alien.png', 16, 16);

        this.load.audio('alien-move', 'assets/audio/alien-move.wav');
        this.load.audio('alien-explode', 'assets/audio/alien-explode.wav');
        this.load.audio('ufo-move', 'assets/audio/ufo.wav');
        this.load.audio('ufo-explode', 'assets/audio/ufo-explosion.wav');
        this.load.audio('player-explode', 'assets/audio/player-explode.wav');

        // Touch plugin
        this.load.image('compass', 'assets/images/touch/compass_rose.png');
        this.load.image('touch_segment', 'assets/images/touch/touch_segment.png');
        this.load.image('touch', 'assets/images/touch/touch.png');
    }

    function create() {

        this.state.start('MainMenu');

    }
    
    return {
        init: init,
        preload: preload,
        create: create
    }
    
};


