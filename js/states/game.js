'use strict';


Invaders.Game = function() {
    
    var lives, liveCount = 3, score = 0, level = 1, isGameOver =false;
    var scoreString = 'Score: ', scoreText, hiScoreString = 'Hi-Score: ', hiScoreText;
    var alienMoveEveryMs = 55 * 16;
    var minTimeBetweenBullets = 200;
    var minTimeBetweenAlienBullets = 2000;
    var minTimeBetweenUfos = 10 * 1000;
    var aliens, player, ufo, bases, bullets, alienBullets, explosions, explosionUfo, wallRight, wallLeft;
    var alienSound, alienExplodeSound, ufoSound, ufoExplodeSound, playerExplodeSound;
    var cursors, joystick, fireButton;
    var alienTimer = 0, alienFireTimer = 0, bulletTimer = 0, ufoTimer = 0;
    var movingRight = true, moveAliensNextRow = false;
    var baseBmps = [], baseDamageBmp;
    var stateText;
    var invadersPerLine = 11;
    var deviceScale = 3;
    var barriers = 4;

    function isLocalStorageAvailable() {

        var test = 'test';
        try {
            localStorage.setItem(test, test);
            localStorage.removeItem(test);
            return true;
        } catch (e) {
            return false;
        }

    }

    function setupDeviceScaling() {

        var widthHnds = Math.floor(game.width / 100);

        if (widthHnds <= 4) {
            deviceScale = 1;
            invadersPerLine = 8;
            barriers = 3;
        } else if (widthHnds < 6) {
            deviceScale = 2;
            invadersPerLine = 11
            barriers = 4;
        } else {
            deviceScale = 3;
            invadersPerLine = 11
            barriers = 4;
        }

    }

    function createVirtualJoystick() {

        game.touchControl = game.plugins.add(Phaser.Plugin.TouchControl);
        game.touchControl.inputEnable();

    }
    
    function createScoreboard() {

        var fontSize = 12 * deviceScale;

        scoreText = game.add.bitmapText(10,10, 'minecraftia',scoreString + score, fontSize);

        stateText = game.add.bitmapText(game.world.centerX, game.world.height / 3, 'minecraftia', '', fontSize);

        if (isLocalStorageAvailable()) {

            if (!localStorage.hiScore) {
                localStorage.hiScore = 1000; // default hi score
            }

            hiScoreText = game.add.bitmapText(10, 10, 'minecraftia', hiScoreString + localStorage.hiScore, fontSize);
            hiScoreText.x = game.width - hiScoreText.width - 10;

        }

        stateText.x = game.width / 2  - stateText.textWidth / 2;

        stateText.visible = false;

    }
    
    function createAlienLayer(id, y) {
        
        for (var x = 1; x <= invadersPerLine; x++) {
            var alien = aliens.create(x * (deviceScale * 16), y, id);
            alien.anchor.setTo(0.5, 0.5);
            alien.body.moves = false;
            alien.scale.setTo(deviceScale);
        }
    }
    
    
    function createAliens() {

        aliens = game.add.group();
        aliens.enableBody = true;
        aliens.physicsBodyType = Phaser.Physics.ARCADE;
        
        var topLine = (game.world.height / 2) - 60 + (level * 20);
        
        createAlienLayer('invader1', topLine);
        createAlienLayer('invader1', topLine - 40);
        createAlienLayer('invader2', topLine - 80);
        createAlienLayer('invader2', topLine - 120);
        createAlienLayer('invader3', topLine - 160);
        
        alienSound = game.add.audio('alien-move');
        alienExplodeSound = game.add.audio('alien-explode');
        
        ufoSound = game.add.audio('ufo-move');
        ufoExplodeSound = game.add.audio('ufo-explode');
        
    }
    
    function createPlayer() {

        var playerX = game.world.width / 2;
        var playerY = (game.world.height / 8) * 7;
        
        player = game.add.sprite(playerX, playerY, 'ship');
        player.anchor.setTo(0.5, 0.5);
        player.animations.add('explode-ship', [ 1, 2, 1, 2, 1, 2, 0 ], 6, false);
        player.scale.setTo(deviceScale);
        game.physics.enable(player, Phaser.Physics.ARCADE);
        player.body.collideWorldBounds = true;

        lives = game.add.group();
        for (var i = 0; i < (liveCount-1); i++) {
            var ship = lives.create(30 + (30 * i), playerY + 20, 'ship');
            ship.anchor.setTo(0.5, 0.5);
            ship.scale.setTo(deviceScale/2);
        }


        playerExplodeSound = game.add.audio('player-explode');
    }

    function createBases() {

        var baseY = (game.world.height / 8) * 7 - 120;
        var distanceBetweenBases = game.width / (barriers + 1); // evenly spaced

        bases = game.add.group();
        bases.enableBody = true;

        baseDamageBmp = game.make.bitmapData(16, 16);
        baseDamageBmp.circle(8, 8, 8, 'rgba(0,27,7,1)');  // rgba(255,0,255,0.2)

        baseBmps = [];

        for (var x = 1; x <= barriers; x++) {
            var baseResize = 32 * deviceScale;
            var baseBmp = game.make.bitmapData(baseResize, baseResize);
            baseBmp.draw('base', 0, 0, baseResize, baseResize); // resize 32 src to 96 target?
            baseBmp.update();
            var baseX = (x * distanceBetweenBases) - (baseResize / 2); // center up
            var base = game.add.sprite(baseX, baseY, baseBmp);
            bases.add(base);
            baseBmps.push({
                bmp: baseBmp,
                worldX: baseX,
                worldY: baseY
            });
        }

    }
    
    
    function createInvisibleWalls() {
        wallLeft = game.add.tileSprite((8*4), 0, 8, game.height, 'blank');
        wallRight = game.add.tileSprite(game.width-(8*6), 0, 8, game.height, 'blank');
        
        game.physics.enable([ wallLeft, wallRight ], Phaser.Physics.ARCADE);
        
        wallLeft.body.immovable = true;
        wallLeft.body.allowGravity = false;
        
        wallRight.body.immovable = true;
        wallRight.body.allowGravity = false;
    }
    
    
    function createBulletGroup(spriteId) {
        var newBulletsGroup = game.add.group();
        newBulletsGroup.enableBody = true;
        newBulletsGroup.physicsBodyType = Phaser.Physics.ARCADE;
        newBulletsGroup.createMultiple(100, spriteId);  // 30 sensible
        newBulletsGroup.setAll('anchor.x', 0.5);
        newBulletsGroup.setAll('anchor.y', 1);
        newBulletsGroup.setAll('outOfBoundsKill', true);
        newBulletsGroup.setAll('checkWorldBounds', true);
        return newBulletsGroup
    }
    
    function createPlayerBullets() {
        bullets = createBulletGroup('bullet-ship');
    }
    
    function createAlienBullets() {
        alienBullets = createBulletGroup('bullet-alien');
        alienBullets.forEach(function (nextBullet) {
           nextBullet.animations.add("fly", [0, 2, 1 ], 10, true);
           nextBullet.scale.setTo(deviceScale*2/3); // bullets at 2/3 device scale
        });
    }
    
    
    function createBullets() {
        createPlayerBullets();
        createAlienBullets();
    }
    
    function createExplosions() {
        explosions = game.add.group();
        for (var x = 0; x < 20; x++) {
            var explosion = explosions.create(0,0, 'explosion', 0, 0, 0, false);
            explosion.anchor.setTo(0, 0);
            explosion.scale.setTo(deviceScale);
            explosion.animations.add('boom', [0, 0], 6, true);
        }
        
    }
    
    
    function createKeyboardControls() {

        cursors = game.input.keyboard.createCursorKeys();
        joystick = game.touchControl.cursors;
        fireButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    }
    
    
    function create() {
        setupDeviceScaling();
        createVirtualJoystick();
        createScoreboard();
        createBases();
        createBullets();
        createExplosions();
        createInvisibleWalls();
        createAliens();
        createPlayer();
        createKeyboardControls();
        ufoTimer = game.time.now + minTimeBetweenUfos;
        game.input.onTap.add(onTapHandler, this);
    }
    
    

    function fireBullet() {
        
        
        if (game.time.now > bulletTimer) {
        
            var bullet = bullets.getFirstExists(false);

            if (bullet)
            {
                //  And fire it
                bullet.reset(player.x, player.y + 8);
                bullet.body.velocity.y = -400;
                bulletTimer = game.time.now + minTimeBetweenBullets;
            }
        }
    }

    
    function moveAliens() {
        alienSound.play();
        alienTimer = game.time.now + alienMoveEveryMs;
        
        // switch the frame to the next move
        aliens.forEachAlive(function(nextAlien) {
            nextAlien.frame = (nextAlien.frame == 0 ? 1 : 0);    
        });
        
        if (moveAliensNextRow) {
            moveAliensDown();
        } else {
            if (movingRight) {
                aliens.x += (deviceScale * 7);
            } else {
                aliens.x -= (deviceScale * 7);
            }
        }
    }
    
    function createUfo() {
        if (ufo) {
            ufo.revive();
            ufo.x = 40;
            ufo.y = 40;
        } else {
            ufo = game.add.sprite(40, 40, 'ufo');
            ufo.anchor.setTo(0.5, 0.5);
            ufo.scale.setTo(deviceScale);
            game.physics.enable(ufo, Phaser.Physics.ARCADE);
            ufo.checkWorldBounds = true;
            ufo.events.onOutOfBounds.add(killUfo, this);
        }
        ufo.body.velocity.x = 180;

        // sometime ufo should go right to left for variety
        var movingLeft = game.rnd.integerInRange(0, 1);
        if (movingLeft) {
            ufo.body.velocity.x *= -1;
            ufo.x = game.width - ufo.x;
        }

        ufoSound.play('', 0, 1, true, true);
        
    }

    function onTapHandler(pointer, doubleTap) {
        if (doubleTap) {
            fireBullet();
        }
    }

    
    function update() {
        
        player.body.velocity.setTo(0, 0);

        if (!isGameOver) {

            if (cursors.left.isDown || joystick.left) {
                player.body.velocity.x = -200;
            }
            else if (cursors.right.isDown || joystick.right) {
                player.body.velocity.x = 200;
            }

            if (fireButton.isDown) { // also there's a tap handler for this to catch doubletaps
                fireBullet();
            }

            if (alienFireTimer < this.game.time.now && aliens.countLiving() > 0) {
                alienFireTimer = this.game.time.now + minTimeBetweenAlienBullets;
                alienFiresBullet();
            }


            if (alienTimer < this.game.time.now && aliens.countLiving() > 0) {
                moveAliens();
            }

            if (ufoTimer < this.game.time.now && aliens.countLiving() > 0) {
                createUfo();
                ufoTimer = this.game.time.now + game.rnd.integerInRange(minTimeBetweenUfos, minTimeBetweenUfos * 2);
            }

            //  Run collision
            game.physics.arcade.overlap(aliens, wallRight, aliensEndOfRowRight, null, this);
            game.physics.arcade.overlap(aliens, wallLeft, aliensEndOfRowLeft, null, this);
            game.physics.arcade.overlap(aliens, bullets, hitAlien, null, this);
            game.physics.arcade.overlap(aliens, bases, aliensReachedBases, null, this);

            game.physics.arcade.overlap(player, alienBullets, hitPlayer, null, this);
            game.physics.arcade.overlap(alienBullets, bullets, hitBullets, null, this);
            game.physics.arcade.overlap(alienBullets, bases, hitBase, null, this);
            game.physics.arcade.overlap(bullets, bases, hitBase, null, this);
            if (ufo && ufo.alive) {
                game.physics.arcade.overlap(bullets, ufo, hitUfo, null, this);
            }
        } else {
            // game over
        }
        
    }
    
    function alienFiresBullet() {
        var alienBullet = alienBullets.getFirstExists(false);
        var aliensAlive = aliens.filter(function(al) {  return al.alive; } , true);
        var random=game.rnd.integerInRange(0,aliensAlive.total-1);

        // randomly select one of them
        var shooter=aliensAlive.list[random];

        aliensAlive.list.forEach(function(nextAlien) {
            if (nextAlien.body.x === shooter.body.x) {
                if (nextAlien.body.y > shooter.body.y) {
                    shooter = nextAlien; // we found one further down the screen
                }
            }
        });

        
        // And fire the bullet from this enemy
        alienBullet.reset(shooter.body.x, shooter.body.y);
        alienBullet.body.velocity.y = 70;
        alienBullet.play('fly');
        
    }
    
    function moveAliensDown() {
        
        aliens.y += 30;
        moveAliensNextRow = false;
        
    }
    
    function aliensEndOfRow() {

        movingRight = !movingRight; // switch direction
        moveAliensNextRow = true;
        
    }
    function aliensEndOfRowRight(theAliens, theWall) {
        
        if (movingRight) {
            aliensEndOfRow();
        } 
    }
    
    function aliensEndOfRowLeft(theAliens, theWall) {
        
        if (!movingRight) {
            aliensEndOfRow();
        }
        
    }
    
    function hitAlien(alien, bullet) {
        alien.kill();
        bullet.kill();

        var alienPoints = 0;
        switch(alien.key) {
            case 'invader1':
                alienPoints = 10;
                break;
            case 'invader2':
                alienPoints = 20;
                break;
            case 'invader3':
                alienPoints = 30;
                break;
        }
        score += alienPoints;
        scoreText.text = scoreString + score;
        
        var explosion = explosions.getFirstExists(false);
        explosion.reset(alien.body.x, alien.body.y);
        explosion.play('boom', null, false, true);
        
        alienExplodeSound.play();
        
        alienMoveEveryMs = aliens.countLiving() * 16 - (level * 5); // speed up as we lose aliens and levels
        

        if (aliens.countLiving() == 0)
        {
            this.gotoNextLevel();
        }
        
    }

    function aliensReachedBases(alien, base) {
        this.gameOver(" YOU ARE INVADED ");
    }

    function gotoNextLevel() {
        score += 1000;
        level += 1;
        this.restart(level);
    }

    function displayNextLetter() {

        stateText.text=  this.message.substr(0, this.counter);
        this.counter += 1;

    }

    function displayLetterByLetterText(message) {

        stateText.text= message;
        stateText.x = game.width / 2  - stateText.textWidth / 2;
        stateText.text = "";
        stateText.visible = true;

        var timerEvent = game.time.events.repeat(100, message.length, displayNextLetter, { message: message, counter: 1 });

        timerEvent.timer.onComplete.add(function() {
            //the "click to restart" handler
            game.input.onTap.addOnce(function() {
                this.restart(); // don't want any args
            }, this);
        }, this);

    }

    function gameOver(message) {

        isGameOver = true;

        if (ufo) {
            ufo.body.velocity.x = 0;
        }
        player.body.velocity.x = 0;

        var newHiScore = false;

        if (isLocalStorageAvailable()) {

            if (score > localStorage.hiScore) {
                localStorage.hiScore = score;
                newHiScore = true;
            }

        }

        var fullMessage = (message ? message : "  GAME OVER ");
        if (newHiScore) {
            fullMessage += "\n\n New Hi Score!";
        }
        fullMessage += "\n\n Click to restart";

        this.displayLetterByLetterText(fullMessage);

        this.sound.stopAll();

        lives.callAll('kill'); // kill off all lives




    }
    
    
    function hitPlayer(alien, bullet) {

        player.play('explode-ship');

        playerExplodeSound.play(); // need a better one for this

        var live = lives.getFirstAlive();

        if (live)
        {
            live.kill();
            alienBullets.callAll('kill'); // kill all bullets for new player
        }

        liveCount -= 1;

        // When the player dies
        if (liveCount < 1)
        {
            this.gameOver();
        }
        
    }
    
    function hitBullets(alienBullet, playerBullet) {
        
        alienBullet.kill();
        playerBullet.kill();
        
    }
    
    function hitBase(alienBullet, base) {
        var baseIdx = bases.getChildIndex(base); 
        
        var matchingBmp = baseBmps[baseIdx];
        
        var bmpRelativeX = Math.round(alienBullet.x - matchingBmp.worldX); 
        var bmpRelativeY = Math.round(alienBullet.y - matchingBmp.worldY); 
        var bmpPixelRgba = matchingBmp.bmp.getPixelRGB(bmpRelativeX, bmpRelativeY); 
        
        if (bmpPixelRgba.r > 0) { // hit a white pixel
            
            matchingBmp.bmp.draw(baseDamageBmp, bmpRelativeX - 8, bmpRelativeY - 8);
            matchingBmp.bmp.update();
            alienBullet.kill();
            
        }
        
        
    }
    
    function killUfo(theUfo) {

        theUfo.kill();
        ufoSound.stop();

    }
    
    function hitUfo(theBullet, theUfo) {
        theBullet.kill();
        killUfo(theUfo);
        
        if (!explosionUfo) {
            explosionUfo = explosions.create(0,0, 'explosion-ufo', 0, 0, 0, false);
            explosionUfo.anchor.setTo(0.5, 0.5);
            explosionUfo.scale.setTo(deviceScale);
            explosionUfo.animations.add('boom', [0, 1], 1, true);
        } else {
            explosionUfo.revive();
        }
        
        explosionUfo.reset(theUfo.body.x, theUfo.body.y); // match our large ufo
        explosionUfo.play('boom', null, false, true);
        
        ufoExplodeSound.play();
        score += 100;
        scoreText.text = scoreString + score;
    }
    
    function render() {
        
    }




    function restart(nextLevel) {
        stateText.visible = false;
        if (!nextLevel) {
            level = 1;
            score = 0;
            liveCount = 3;
        }

        if (ufo) {
            ufo.kill();
            ufo.destroy();
            ufo = null;
        }


        [ bullets, aliens, alienBullets, bases, lives].every(function(group) {
            group.destroy();
        });

        lives == null;
        movingRight = true;

        // and destroy all the sounds
        this.sound.destroy();

        isGameOver = false;
        alienMoveEveryMs = 55 * 16;
        game.world.removeAll();

        this.create();
    }
    
    return {
        create: create,
        update: update,
        render: render,
        gotoNextLevel: gotoNextLevel,
        gameOver: gameOver,
        displayLetterByLetterText: displayLetterByLetterText,
        restart: restart
    }
    
};


