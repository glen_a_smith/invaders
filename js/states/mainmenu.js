'use strict';

Invaders.MainMenu = function(game) {

    function create() {

        this.stage.backgroundColor = 0x001b07;

        var eighthScreen = Math.floor(game.height / 8);
        var fontHeight = Math.floor(game.height / 15);
        var logoScale = game.height / 25;

        var logo = this.add.image(this.world.centerX, eighthScreen * 4, 'logo');
        logo.animations.add('move', [0, 1], 0.5, true);
        logo.anchor.x = 0.5;
        logo.anchor.y = 0.5;
        logo.scale.setTo(logoScale);
        logo.animations.play('move');

        var title = "Glen's Invaders";
        var instructions = game.device.desktop ? "Use Cursors & Spacebar" : "Tap & Drag To Play";

        var titleText = game.add.bitmapText(game.world.centerX, eighthScreen * 1, 'minecraftia', title, fontHeight);
        titleText.x = game.width / 2  - titleText.textWidth / 2;

        var clickText = game.add.bitmapText(game.world.centerX, game.world.centerY - 25, 'minecraftia', instructions, fontHeight);
        clickText.x = game.width / 2  - clickText.textWidth / 2;
        clickText.y = eighthScreen * 6;

        this.input.onDown.addOnce(this.start, this);

        titleText.text = "";
        clickText.text = "";

        displayLetterByLetterText(titleText, title, function() {
            displayLetterByLetterText(clickText, instructions, function() {

            })
        });

    }


    function displayNextLetter() {

        this.textObject.text=  this.message.substr(0, this.counter);
        this.counter += 1;

    }

    function displayLetterByLetterText(textObject, message, onCompleteCallback) {

        var timerEvent = game.time.events.repeat(80, message.length, displayNextLetter, { textObject: textObject, message: message, counter: 1 });

        timerEvent.timer.onComplete.addOnce(onCompleteCallback, this);

    }




    function start() {

        this.state.start('Game');

    }
    
    return {
        create: create,
        start: start,
        displayLetterByLetterText: displayLetterByLetterText
    }
    
};


