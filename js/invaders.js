'use strict';

var game = new Phaser.Game(window.innerWidth, window.innerHeight, Phaser.AUTO, '', null, false, true);

game.state.add('Boot', Invaders.Boot);
game.state.add('Preloader', Invaders.Preloader);
game.state.add('MainMenu', Invaders.MainMenu);
game.state.add('Game', Invaders.Game);

game.state.start('Boot');

