# Glen's legit Space Invaders clone

This is my first crack at writing a full JavaScript game in [Phaser.io](http://phaser.io/).

The goal was to write a complete Space Invaders clone (with sound) to get a feel for all the APIs.

And it happened! 

You can [play it here](http://blogs.bytecode.com.au/projects/invaders/), then come back here to this repo to check out the source!


Or just enjoy an animated gif of the action below... 

![Glen's Invaders in Action](http://blogs.bytecode.com.au/glen/images/2015/invaders-final.gif) 